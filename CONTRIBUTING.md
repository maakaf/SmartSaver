# Welcome

Hello! Thanks for taking an interest in this project and code :)

Contributions to this project are welcome of course, otherwise it wouldn't reside on GitHub 😃 however there's a few things to be aware of:

- It might take a long time for the maintainer(s) to reply to issues or review PRs, they will have have a day jobs & might not have looked at the code for a while.
- The code here is likely to not be bullet proof & production grade, there might be a lack of unit tests or other practices missing from the code base.

# Contributing

There's several ways of contributing to this project, and effort has been made to make this as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features
- Becoming a maintainer

Some PR guidance:

- Please keep PRs small and focused on a single feature or change, with discreet commits. Use multiple PRs if need be.
- If you're thinking of adding a feature via a PR please create an issue first where it can be discussed.

**Great Bug Reports** tend to have:

- A quick summary and/or background
- Steps to reproduce
  - Be specific!
  - Give sample code if you can. Even if it's a snippet
- What you expected would happen
- What actually happens
- Notes (possibly including why you think this might be happening, or stuff you tried that didn't work)

